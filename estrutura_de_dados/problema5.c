/* Universidade Federal da Fronteira Sul
Aluno: Luan Felix Pimentel

Problema 5 -  Leia um numero real e imprima a quinta parte deste numero.
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
  float x;
  scanf("%f", &x);
  x = x/5;
  printf("a quinta parte eh: %.2f", x);
  return 0;
}
