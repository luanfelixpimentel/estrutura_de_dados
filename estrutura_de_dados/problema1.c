/* Universidade Federal da Fronteira Sul
Aluno: Luan Felix Pimentel
Estutura de Dados

Fazer um programa que leia um numero inteiro e o imprima
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
  int a;
  scanf("%d", &a);
  printf("%d\n", a);
  return 0;
}
