/* Universidade Federal da Fronteira Sul
Aluno: Luan Felix Pimentel

Problema 3 - Peca ao usuario para digitar tres valores inteiros e imprima a soma deles.

*/

#include <stdio.h>
#include <stdlib.h>

int main(){
  int x, y, z;
  printf(" Digite tres numeros inteiros para ver a soma deles\n");
  scanf("%d", &x); scanf("%d", &y); scanf("%d", &z);
  x = x+y+z;
  printf("A soma dos numeros inteiros sao: %d \n", x);
  return 0;
}
