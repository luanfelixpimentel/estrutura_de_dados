/*  Universidade Federal da Fronteira Sul
Aluno: Luan Felix Pimentel

Leia uma temperatura em graus Celsius e apresente-a convertida em graus Fahrenheit.A formula de conversao
e:F=C∗(9.0/5.0)+32.0, sendo F a temperatura em Fahrenheit e C a temperatura em Celsius.

*/

#include <stdio.h>
#include <stdlib.h>

int main(){
  float celsius, fahrenheit;
  printf ("Digite a temperatura em graus Celsius: \n");
  scanf("%f", &celsius);
  fahrenheit = celsius *((9.0/5.0) + 32.0);
  printf("A temperatura em fahrenheit eh de: %.2f", fahrenheit);
  return 0;
}
