/*  Universidade Federal da Fronteira Sul
Aluno:Luan Felix Pimetel

Faca um programa que leia um numero real e o imprima

*/

#include <stdio.h>
#include <stdlib.h>

int main(){
  float x;
  printf("Insira um numero real: \n");
  scanf("%f", &x);
  printf("O numero real x inserido foi:\n %.2f", x);
  return 0;
}
