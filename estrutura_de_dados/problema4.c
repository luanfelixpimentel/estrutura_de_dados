/* Universidade Federal da Fronteira Sul
Aluno: Luan Felix Pimentel

Leia um numero real e imprima o resultado do quadrado desse numero.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
  float x;
  scanf("%f", &x);
  x = x*x;
  printf("%.2f \n", x);
  return 0;
}
